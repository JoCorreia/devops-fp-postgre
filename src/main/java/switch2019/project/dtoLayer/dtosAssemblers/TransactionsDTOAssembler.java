package switch2019.project.dtoLayer.dtosAssemblers;

import switch2019.project.dtoLayer.dtos.TransactionDTOout;
import switch2019.project.dtoLayer.dtos.TransactionsDTO;

import java.util.List;

public class TransactionsDTOAssembler {

    public static TransactionsDTO createDTOFromPrimitiveTypes(List<TransactionDTOout> transactions) {

        TransactionsDTO transactionsDTO = new TransactionsDTO(transactions);
        return transactionsDTO;
    }
}
