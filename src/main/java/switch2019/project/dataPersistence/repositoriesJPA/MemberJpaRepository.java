package switch2019.project.dataPersistence.repositoriesJPA;


import org.springframework.data.repository.CrudRepository;
import switch2019.project.dataModelLayer.dataModel.MemberJpa;

import java.util.List;

public interface MemberJpaRepository extends CrudRepository<MemberJpa, Long> {

    MemberJpa findById(long id);
    //List<MemberJpa> findAllByGroupId( GroupId id);
    List<MemberJpa> findAll();
}
