package switch2019.project.dataPersistence.repositoriesJPA;


import org.springframework.data.repository.CrudRepository;
import switch2019.project.dataModelLayer.dataModel.AddressJpa;

import java.util.List;
import java.util.Optional;

public interface AddressJpaRepository extends CrudRepository<AddressJpa, Long> {

    Optional<AddressJpa> findById(long id);
    List<AddressJpa> findAll();
}
