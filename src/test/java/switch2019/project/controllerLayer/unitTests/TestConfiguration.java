package switch2019.project.controllerLayer.unitTests;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import switch2019.project.applicationLayer.applicationServices.*;

/**
 * @author Ala Matos
 */
@Profile("test")
//Profile annotation is a logical grouping that can be activated programmatically. It can be used on type-level
// annotation on any class or it can be used as a meta-annotation for composing custom stereotype annotations or
// as a method-level annotation on any @Bean method. Essence is, @Profile is used to conditionally activate/register.
//----------------------------------------------------------------------------------------------------------------
@Configuration //indicates that the class will be used by JavaConfig as a source of bean definitions.
//An application may make use of just one @Configuration-annotated class, or many.

public class TestConfiguration {

//    @Bean //The objects that form the backbone of your application and that are managed by the Spring
    // IoC container are called beans. A bean is an object that is instantiated, assembled, and otherwise
    // managed by a Spring IoC container. These beans are created with the configuration metadata that you
    // supply to the container.
//    @Primary
//    public USminus1_service USminus1() {
//        return Mockito.mock(USminus1_service.class);
//    }

//    @MockBean
//    private ServiceToMock serviceToMock;

    @MockBean
    private US001CheckIfSiblingsService us001CheckIfSiblingsService;

    @MockBean
    public US002_1CreateGroupService us002_1CreateGroupService;

    @MockBean
    public US003AddPersonToGroupService uS003AddPersonToGroupService;

    @MockBean
    public US004GroupsThatAreFamilyService us004GroupsThatAreFamilyService;

    @MockBean
    public US005CreatePersonCategoryService us005CreatePersonCategoryService;

    @MockBean
    public US005_1CreateGroupCategoryService us005_1CreateGroupCategoryService;

    @MockBean
    public US006CreatePersonAccountService us006CreatePersonAccountService;

    @MockBean
    public US007CreateGroupAccountService us007CreateGroupAccountService;

    @MockBean
    public US008CreatePersonTransactionService us008CreatePersonTransactionService;

    @MockBean
    public US008_1CreateGroupTransactionService us008_1CreateGroupTransactionService;

    @MockBean
    public CreatePersonService createPersonService;

    @MockBean
    public US010PersonSearchAccountRecordsService us010PersonSearchAccountRecordsService;

    @MockBean
    public US010GroupSearchAccountRecordsService us010GroupSearchAccountRecordsService;

}
