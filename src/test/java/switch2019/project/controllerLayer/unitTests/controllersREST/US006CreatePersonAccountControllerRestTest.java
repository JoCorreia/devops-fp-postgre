package switch2019.project.controllerLayer.unitTests.controllersREST;


import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import switch2019.project.applicationLayer.applicationServices.US006CreatePersonAccountService;
import switch2019.project.dtoLayer.dtos.CreatePersonAccountDTO;
import switch2019.project.dtoLayer.dtos.NewPersonAccountInfoDTO;
import switch2019.project.dtoLayer.dtos.PersonDTO;
import switch2019.project.dtoLayer.dtosAssemblers.CreatePersonAccountDTOAssembler;
import switch2019.project.dtoLayer.dtosAssemblers.PersonDTOAssembler;
import switch2019.project.controllerLayer.controllers.controllersREST.US006CreatePersonAccountControllerREST;
import switch2019.project.controllerLayer.integrationTests.AbstractTest;
import switch2019.project.domainLayer.domainEntities.aggregates.person.Birthdate;
import switch2019.project.domainLayer.domainEntities.aggregates.person.Birthplace;
import switch2019.project.domainLayer.domainEntities.aggregates.person.Name;
import switch2019.project.domainLayer.domainEntities.vosShared.Email;
import switch2019.project.domainLayer.domainEntities.vosShared.PersonID;
import switch2019.project.domainLayer.exceptions.InvalidArgumentsBusinessException;
import switch2019.project.domainLayer.exceptions.NotFoundArgumentsBusinessException;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class US006CreatePersonAccountControllerRestTest extends AbstractTest {

    @Mock
    private US006CreatePersonAccountService service;
    @Autowired
    private US006CreatePersonAccountControllerREST controller;

    // SUCCESS

    @Test
    public void whenPersonAccountIsCreated_thenRetrievedMsgIsSuccess() {

        // Arrange

            // Arrange Person
        final String personEmail = "manuel@gmail.com";
        final String personName = "Fontes";
        final LocalDate personBirthdate = LocalDate.of(1964, 02, 16);
        final String personBirthplace = "Vila Nova de Gaia";

            // Arrange Account
        final String accountDescription = "Tickets for Roland Garros";
        final String accountDenomination = "Tennis";

        // Expected result
        Email email = Email.createEmail(personEmail);
        Name name = Name.createName(personName);
        Birthdate birthdate = Birthdate.createBirthdate(personBirthdate);
        Birthplace birthplace = Birthplace.createBirthplace(personBirthplace);
        PersonID fatherID = null;
        PersonID motherID = null;

        PersonDTO isAccountCreatedExpected = PersonDTOAssembler.createDTOFromDomainObject(email, name, birthdate, birthplace, fatherID, motherID);

        NewPersonAccountInfoDTO newPersonAccountInfoDTO = new NewPersonAccountInfoDTO(accountDescription, accountDenomination);

        CreatePersonAccountDTO createPersonAccountDTO = CreatePersonAccountDTOAssembler.createDTOFromPrimitiveTypes(personEmail, accountDescription, accountDenomination);

        // Expected Response Entity
        ResponseEntity<Object> expectedResponse = new ResponseEntity<>(isAccountCreatedExpected, HttpStatus.CREATED);

        // Mock the behaviour of the service's createAccount method, so it does not depend on other parts (e.g. DB)
        Mockito.when(service.createAccount(createPersonAccountDTO)).thenReturn(isAccountCreatedExpected);

        // Act
        ResponseEntity<Object> isAccountCreated = controller.createPersonAccount(newPersonAccountInfoDTO, personEmail);

        //       Expected results
        Object isAccountCreatedStatusCodeValue = isAccountCreated.getStatusCodeValue();
        Object isAccountCreatedHeaders = isAccountCreated.getHeaders().toString();
        Object isAccountCreatedResponseBody = isAccountCreated.getBody().toString();
        Object expectedStatusCodeValue = expectedResponse.getStatusCodeValue();
        Object expectedHeaders = expectedResponse.getHeaders().toString();
        Object expectedResponseBody = expectedResponse.getBody().toString();

        //Assert
        assertEquals(expectedStatusCodeValue,isAccountCreatedStatusCodeValue);
        assertEquals(expectedHeaders,isAccountCreatedHeaders);
//        assertEquals(expectedResponseBody,isAccountCreatedResponseBody);
    }



    // ACCOUNT_ALREADY_EXIST - using account denomination & description from "Bootstrapping"

    @Test
    public void whenPersonAccountIsCreated_thenRetrievedMsgIsAccountAlreadyExists() {

        // Arrange

            // Arrange Person
        final String personEmail = "manuel@gmail.com";
        final String accountDescription = "Personal bank account";
        final String accountDenomination = "Bank Account";

        NewPersonAccountInfoDTO newPersonAccountInfoDTO = new NewPersonAccountInfoDTO(accountDescription, accountDenomination);

        CreatePersonAccountDTO createPersonAccountDTO = CreatePersonAccountDTOAssembler.createDTOFromPrimitiveTypes(personEmail, accountDescription, accountDenomination);

        // Mock the behaviour of the service's createAccount method, so it does not depend on other parts (e.g. DB)
        Mockito.when(service.createAccount(createPersonAccountDTO)).thenThrow(new InvalidArgumentsBusinessException(US006CreatePersonAccountService.ACCOUNT_ALREADY_EXIST));

        // Act
        Throwable thrown = assertThrows(InvalidArgumentsBusinessException.class, () -> controller.createPersonAccount(newPersonAccountInfoDTO, personEmail));

        // Assert
        assertEquals(thrown.getMessage(), US006CreatePersonAccountService.ACCOUNT_ALREADY_EXIST);
    }


    // PERSON_DOES_NOT_EXIST

    @Test
    public void whenPersonAccountIsCreated_thenRetrievedMsgIsPersonDoesNotExists() {

        // Arrange
        String personEmail = "lemos@yahoo.com";
        String accountDescription = "Lemos Family sports expenses";
        String accountDenomination = "Tennis";

        NewPersonAccountInfoDTO newPersonAccountInfoDTO = new NewPersonAccountInfoDTO(accountDescription, accountDenomination);

        CreatePersonAccountDTO createPersonAccountDTO = CreatePersonAccountDTOAssembler.createDTOFromPrimitiveTypes(personEmail, accountDescription, accountDenomination);

        // Mock the behaviour of the service's createAccount method, so it does not depend on other parts (e.g. DB)
        Mockito.when(service.createAccount(createPersonAccountDTO)).thenThrow(new NotFoundArgumentsBusinessException(US006CreatePersonAccountService.PERSON_DOES_NOT_EXIST));

        // Act
        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> controller.createPersonAccount(newPersonAccountInfoDTO, personEmail));

        //Assert
        assertEquals(thrown.getMessage(), US006CreatePersonAccountService.PERSON_DOES_NOT_EXIST);
    }

}

